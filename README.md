# Crontab How-to



## Introduction

Welcome!

This GitLab repo contains guided tutorials on how to interact with and make the most of Cron, a Unix/Unix-like command-line utility.


## What is it?
Cron is a *nix (Unix/Unix-like) command-line utility used for scheduling tasks to run periodically. It bears similar functionality and purpose to other tools such as Windows Task Scheduler, but for *nix systems.

You will learn more about the specific details of Cron starting with Lesson 1, and more advanced functionality will be covered throughout the subsequent lessons.


## Requirements
To follow along with these lessons, you will need access to either of the following:

    - A *nix operating system with the Cron command-line utility
    
    - A Windows device with Windows Subsystem for Linux (WSL) installed

In cases where `sudo` privileges are required, if you don't have access to these privileges, you may skip past the subset of instructions that require them.


## Windows setup
By default, when installing a Linux distro in WSL, cron will not run upon logging in to Windows. This is because WSL itself doesn't launch on boot, or even upon logging in to Windows, so command-line utilities provided through WSL will also not run automatically. The following tutorial from How-To Geek will help walk you through the steps to get WSL (and cron) to run automatically on boot on Windows devices:

https://www.howtogeek.com/746532/how-to-launch-cron-automatically-in-wsl-on-windows-10-and-11/


## Author and Acknowledgements

### Repository and lesson planning
Gabriel Internoscia

### Sources
https://www.howtogeek.com/746532/how-to-launch-cron-automatically-in-wsl-on-windows-10-and-11/<br>
https://www.wikihow.com/Set-up-a-Crontab-File-on-Linux<br>
https://serverfault.com/questions/162388/what-does-five-asterisks-in-a-cron-file-mean<br>
https://devconnected.com/bash-if-else-syntax-with-examples/<br>
https://askubuntu.com/questions/925621/how-to-add-a-bash-script-to-a-cron-job<br>
https://serverfault.com/questions/247043/what-user-do-scripts-in-the-cron-folders-run-as-i-e-cron-daily-cron-hourly<br>

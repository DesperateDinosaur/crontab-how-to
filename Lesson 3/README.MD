# Crontab Lesson 3



## Goal(s)
- Learn how to create cron jobs that run multiple commands
- Learn how to create cron jobs that run bash scripts


## Before we begin
Up until now, we've created cron jobs that run individual commands, but this isn't practical when we want to accomplish a task that involves multiple commands. We can't make multiple cron jobs since they would clutter up the file. Furthermore, if a given set of commands are meant to be run in a particular order, then by making multiple jobs and setting the time to be the same for each job, there's no guarantee that the commands will be run in the correct order, just that they'll be queued up to run at the same time.

In this lesson, we will look at creating multi-command cron jobs for tasks that involve running a few basic commands in a specific order, and then we'll look at running bash scripts for more advanced tasks that can involve many different commands. With the features of bash scripting, these jobs can also be much more powerful, utilizing operators (`>`, `++`, `=`) and conditional statements (`x < y`, `length == 0`).

To see a completed solution, look for the file labeled "solution" in the <b>Lesson 3</b> directory of this repo.


## 3.1: Creating multi-command cron jobs
Creating a multi-command cron job is really simple:
    <br>&nbsp;&nbsp;&nbsp;&nbsp;1. We start by creating a cron job as usual.
    <br>&nbsp;&nbsp;&nbsp;&nbsp;2. For the command field, we write out our first command, then append two ampersands (`&&`) onto it:
        <br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; `echo hello &&` <---- semicolon
    <br>&nbsp;&nbsp;&nbsp;&nbsp;3. After this command, we simply write out our next command:
        <br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; `echo hello && echo goodbye` <---- next command

Each individual command will then be executed in order at the specified time.

Let's put it into practice now:
    <br>&nbsp;&nbsp;&nbsp;&nbsp;1. Within your terminal window (open a new one if none are open), type in the command to edit your crontab
    <br>&nbsp;&nbsp;&nbsp;&nbsp;2. Remove any existing jobs and then add the following line into the crontab file:
        <br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;`*/1 * * * * echo command1 && echo command2`
    <br>&nbsp;&nbsp;&nbsp;&nbsp;3. Save the changes you've made to the file and exit out of your editor back to your CLI

After deleting your old cron jobs and inserting this new one, your crontab file should look like this:

![Lesson 3.1 screenshot](./images/lesson_3.1.png)

Just like in the terminal, we can put `&&` in between two commands to have them run one after another. The order of the commands running will be kept, so command1 should always print out before command2. Now wait one minute while cron does it's thing, and observe the results in the terminal.

---
### Question:
Another way of specifying two commands to run in one job is by separating them with a semicolon (`;`), so a job would look like this:

`*/1 * * * * echo command1; echo command2`

Research: what is the difference between `&&` and `;` when running two commands at once?

### Answer:
<p>
<details>
<summary>Click to see the answer</summary>

Two commands separated by `&&` means that each command depends on the one before it executing successfully. This means the commands will always run in the order that they are typed out, and if an earlier command fails, the ones following after it won't run.

Conversely, semicolons `;` are used when separating multiple commands to run them all at once. These commands <b>aren't</b> dependant on each other, so they may execute out of order, and if one fails, the other commands will still attempt to execute.

</details>
</p>

---


## 3.2: Creating and running a bash script through a cron job
Sometimes, you need a little bit more power out of your cron jobs. It's not really elegant to keep chaining multiple commands with `&&` or `;` when you want to run many, many more commands. Furthermore, you're limited to typing commands out like you would in the terminal, which isn't super elegant. Instead, we can create a 📜 bash script ✍️

For this lesson, we're going to create a simple bash script that tells the user whether they are root or not. We will then modify our multi-command to run this bash script.

Let's get started:
    <br>&nbsp;&nbsp;&nbsp;&nbsp;1. Create a new file called `isRoot.sh`:
        <br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;`touch isRoot.sh`
        <br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Note</b>: you can make this file anywhere on your system, just take note of the filepath and ensure that it's in an easy-to-access place!
    <br>&nbsp;&nbsp;&nbsp;&nbsp;2. Open this file in your text editor:
        <br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;`vim isRoot.sh`
        <br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Note</b>: you may replace `vim` with whichever text editor you want.
    <br>&nbsp;&nbsp;&nbsp;&nbsp;3. Add the following code into the file:<br>
    ```
    bash
    #!/bin/bash
    if [ $UID  -eq  0 ]
    then
      echo "Script run as root"
    else
      echo "Script run as non-root"
    fi
    ```
    <br>&nbsp;&nbsp;&nbsp;&nbsp;4. Save the changes you've made to the file and exit out of your editor back to your CLI

![Lesson 3.2 screenshot 1](./images/lesson_3.2.1.png)

Let's walk through what this script does:

```bash
if [ $UID  -eq 0 ]
```
<br>&nbsp;&nbsp;&nbsp;&nbsp;We are checking if the UID of the user running the script is `0`, which is the UID of the `root` user

```bash
then
  echo "Script run as root"
```
<br>&nbsp;&nbsp;&nbsp;&nbsp;If the UID <b>is</b> `0` (meaning we are running the script as root), then print out yes.

```bash
else
  echo "Script run as non-root"
fi
```
<br>&nbsp;&nbsp;&nbsp;&nbsp;If the UID is <b>not</b> `0` (meaning we are running the script as a <b>non</b>-root user), then print out no. Finish the script.

You can test out this bash script quickly by typing `bash isRoot.sh` into your terminal and hitting enter
<b>Note</b>: this assumes you're in the same directory as the bash script. If not, swap `isRoot.sh` out for the path to the script.

You should see a message print out in the terminal:
    <br>&nbsp;&nbsp;&nbsp;&nbsp;`Script run as non-root`

If you want, you can also type `sudo bash isRoot.sh` (provided you have sudo privileges) and hit enter, where you should see the following message print instead:
    <br>&nbsp;&nbsp;&nbsp;&nbsp;`Script run as root`

Now, let's use this script in a cron job:
    <br>&nbsp;&nbsp;&nbsp;&nbsp;1. Within your terminal window (open a new one if none are open), type in the command to edit your crontab
    <br>&nbsp;&nbsp;&nbsp;&nbsp;2. Modify the existing cron job to replace the commands with a call to run your bash script:
        <br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;`*/1 * * * * echo command1 && echo command2` --- becomes --> `*/1 * * * * /bin/bash <script path>`
        <br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Note</b>: replace `<script path>` with the absolute path to your script file.
    <br>&nbsp;&nbsp;&nbsp;&nbsp;3. Save the changes you've made to the file and exit out of your editor back to your CLI

![Lesson 3.2 screenshot 2](./images/lesson_3.2.2.png)

There are several things to notea about using `bash` or `/bin/bash` to run the script:
    <br>&nbsp;&nbsp;&nbsp;&nbsp;1. Bash script permissions set up by default to allow the file to run through bash. If you try and run the file directly with `isRoot.sh` for example, you'll get permission issues. You can modify permissions using `sudo chmod` but this requires `sudo` permissions.
    <br>&nbsp;&nbsp;&nbsp;&nbsp;2. We have to specify full paths inside the cron job (`bin/bash`, `<script path>`). This is because crontab executes our commands from a different location on the system than where we are.

With all that out of the way, wait for the next minute to pass and you should see the following print out in your console:
    <br>&nbsp;&nbsp;&nbsp;&nbsp;`Script run as non-root`

---
### Question:
Why do you think the bash script believes we aren't running it as root?

### Answer:
<p>
<details>
<summary>Click to see the answer</summary>

We've created this cron job within our personal crontab, which means it's running under our account. If we wanted to run the script as root (and therefore have it print out true), then we would have to create this job in the system crontab instead.

</details>
</p>

---

And that's it! Through these three lessons, you've (hopefully) learned how to create cron jobs, what each number means in a cron job (and how they work), how to edit system cron jobs as well as cron jobs of other users on the system, and finally how to execute multiple commands in one cron job through multi-command jobs and bash script jobs.

Thank you so very much for reading through this tutorial. These are just the basics; There's plenty more to learn about cron jobs, so keep experimenting and exploring!

## Solution
To see a completed solution, look for the file labeled "solution" in the <b>Lesson 3</b> directory of this repo.
# Crontab Lesson 2



## Goal(s)
- Learn how to edit the system-wide crontab file <b>(requires sudo)</b>
- Observe user-specific and system-wide crontab jobs running together <b>(requires sudo)</b>
- Learn how to edit other users' crontab files <b>(requires sudo)</b>


## Before we begin
System-wide cron jobs should be treated with care, as they run for any users logged in to the system, and as such can become an annoyance if other users' workflows become impacted by whatever jobs you set up. Ensure that any jobs you create in the system-wide crontab are jobs that <i>really</i> need to be run system-wide. If it's only for your gain, you should probably put it in your personal crontab instead.

Cron uses the timezone of the system it's running on to determine when to run jobs. For users from different timezones, this means jobs may run at different times than what you might think due to differences in timezones, so if you plan to use cron on a system like this, take the system's timezone into account!

To see a completed solution, look for the file labeled "solution" in the <b>Lesson 2</b> directory of this repo.


## 2.1: Editing the system-wide crontab file
The system-wide crontab file, as explained in lesson 1, is located at `/etc/crontab`. This file is a special edge case, as it can't really be modified using the `crontab` command. Instead, we're going to have to manually edit the file ourselves with sudo privileges. This will be the only time we manually open the file for editing instead of going through the `crontab` command.


To start:
    <br>&nbsp;&nbsp;&nbsp;&nbsp;1. Navigate to the '/etc' dirctory with the following command:
        <br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;`cd /etc`
    <br>&nbsp;&nbsp;&nbsp;&nbsp;2. Within this directory, type in the following command to begin editing the file:
        <br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;`sudo vim crontab`
        <br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Note</b>: you may replace `vim` with whichever text editor you want.

Now that we have the crontab file open, we're going to create our own job, save it, and watch it run:
    <br>&nbsp;&nbsp;&nbsp;&nbsp;1. Enter the following line into the crontab file you're currently editing:
        <br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;`*/2 * * * * echo system-hello`
    <br>&nbsp;&nbsp;&nbsp;&nbsp;2. Save the changes you've made to the file and exit out of your editor back to your CLI

![Lesson 2.1 screenshot](./images/lesson_2.1.png)

What we've just created is a cron job that will now run automatically while the computer is on, regardless of who is logged in.

---
### Question:
Why is it preferrable to edit crontab files through the `crontab` command rather than directly by opening them through text editors? (Hint: the answer is in the Lesson 1 completed solution file!)

### Answer:
<p>
<details>
<summary>Click to see the answer</summary>

The crontab utility can check for erroneous syntax and incorrectly created jobs.
<br>(✨Bonus answer✨ the crontab command also makes it easier to edit user-specific files by passing the username as a parameter to the command!)

</details>
</p>

---

Like with lesson 1, you can wait for this system-wide job to run every 2 minutes and observe the output in the terminal. As you do, you may also notice your other job run sometimes as well, in-between runs of your system-wide job. This brings us to the next section...


## 2.2: Observing the difference between user-specific and system-wide cron jobs
To illustrate the difference between user-specific cron jobs and system-wide cron jobs, we're going to switch over to a different acount. If you have access to another account on your system, you can use that. If you don't have access to one, then create a new user account.

Log out of your user account fully and login to this other account. Open up a new terminal window and wait for your jobs to run. You may notice that system-hello appears in the terminal every 2 minutes as expected, but none of the other jobs set up for your user account from earlier will run.

---
### Question:
Research: why do system-wide cron jobs exist?

### Answer:
<p>
<details>
<summary>Click to see the answer</summary>

System-wide cron jobs are typically used to schedule tasks relating to system services.

</details>
</p>

---

## 2.3: Creating cron jobs for other users
The cronjob command provides a clean and easy way to modify the crontab files of other users on the system, regardless of whether they are logged in or not. In this section, you're goiing to be using your primary account and the alternate account to create cronjobs across users.

If you are still logged in to the alternate account, log out fully and log in to your primary account.
<br>From here:
    <br>&nbsp;&nbsp;&nbsp;&nbsp;1. Open the crontab window of the alternate account in the terminal:
        <br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;`sudo crontab -u <alternate username> -e`
    <br>&nbsp;&nbsp;&nbsp;&nbsp;2. We will be creating a new cron job for this user! On a new line, write the following:
        <br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;`*/7 * * * * echo alternate-hello`
    <br>&nbsp;&nbsp;&nbsp;&nbsp;3. Save the changes you've made to the file and exit out of your editor back to your CLI

![Lesson 2.3 screenshot](./images/lesson_2.3.png)

We've now just created our first cron job across users. Editing the cron jobs of another user is useful when that user has running jobs that are taking up many resources, or simply when someone asks you to modify their job for you while they're away 😜

Wait for the results. You should see the cron jobs for your account and the system-wide cron jobs running together, but not the alternate user cron jobs.
<br>Now, log out of your account and log in to the alternate account. Open up a new terminal window and wait for the results. You should see the jobs running for the <i>alternate</i> account this time and <b>not</b> your <i>primary</i> account.

We're going to take a look at the changes we've made across accounts:
    <br>&nbsp;&nbsp;&nbsp;&nbsp;1. Open the crontab window of the alternate account in the terminal:
        <br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;`crontab -u <alternate username> -e`
        <br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Note</b>: we aren't prepending `sudo` to our command this time because we aren't modifying crontabs across users since at this point we're now logged in <i>as</i> the alternate account.
    <br>&nbsp;&nbsp;&nbsp;&nbsp;2. Check to see the cronjob you added here from your primary account. It should look like the above screenshot.

And that's all there is to modifying cronjobs across users. You need sudo privileges to modify the crontab files of users other than whoever is currently logged in. Also, be sure to not manipulate other users' crontabs at random, or else you might make them very mad ‼️

Now, clear out the cron jobs you've made from the system crontab and the alternate user crontab. We don't want our cron jobs to get too cluttered, and we still have one more Lesson to checkout!

---

If you've gotten the hang of using sudo to edit the different kinds of crontabs (or in case you don't have sudo privileges and are skipping ahead), check out Lesson 3 next. It's going to teach you about running multiple tasks in one job!

## Solution
To see a completed solution, look for the file labeled "solution" in the <b>Lesson 2</b> directory of this repo.